// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef XIO_STREAM_SCANNER_H
#define XIO_STREAM_SCANNER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct XioStreamScannerState * XIO_STREAM_SCANNER_HANDLE;

extern
XIO_STREAM_SCANNER_HANDLE
xio_stream_scanner_create (
	void
);

extern
void
xio_stream_scanner_destroy (
	XIO_STREAM_SCANNER_HANDLE handle_
);

extern
int
xio_stream_scanner_init (
	XIO_STREAM_SCANNER_HANDLE handle_,
	const uint8_t * query_,
	const size_t query_length_
);

extern
bool
xio_stream_scanner_matched (
	XIO_STREAM_SCANNER_HANDLE handle_
);

extern
int
xio_stream_scanner_process_stream (
	XIO_STREAM_SCANNER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

#endif // XIO_SCANNER_H
