// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef UARTIO_H
#define UARTIO_H

#include <stdint.h>

#include "azure_c_shared_utility/xio.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct UARTIO_CONFIG_TAG
{
	uint32_t baud_rate;
	size_t ring_buffer_size;
} UARTIO_CONFIG;

extern
int
uartio_close (
	CONCRETE_IO_HANDLE uartio_,
	ON_IO_CLOSE_COMPLETE on_io_close_complete_,
	void * callback_context_
);

extern
CONCRETE_IO_HANDLE
uartio_create (
	void * io_create_parameters_
);

extern
void
uartio_destroy (
	CONCRETE_IO_HANDLE uartio_
);

extern
void
uartio_dowork (
	CONCRETE_IO_HANDLE uartio_
);

extern
const IO_INTERFACE_DESCRIPTION *
uartio_get_interface_description (
	void
);

extern
int
uartio_open (
	CONCRETE_IO_HANDLE uartio_,
	ON_IO_OPEN_COMPLETE on_io_open_complete_,
	void * on_io_open_complete_context_,
	ON_BYTES_RECEIVED on_bytes_received_,
	void * on_bytes_received_context_,
	ON_IO_ERROR on_io_error_,
	void * on_io_error_context_
);

extern
OPTIONHANDLER_HANDLE
uartio_retrieveoptions (
	CONCRETE_IO_HANDLE uartio_
);

extern
int
uartio_send (
	CONCRETE_IO_HANDLE uartio_,
	const void * const buffer_,
	const size_t buffer_size_,
	ON_SEND_COMPLETE on_send_complete_,
	void * callback_context_
);

extern
int
uartio_setoption (
	CONCRETE_IO_HANDLE uartio_,
	const char * const option_name_,
	const void * const option_value_
);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* UARTIO_H */

