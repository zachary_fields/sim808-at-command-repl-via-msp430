// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef CORE_MSP430_H
#define CORE_MSP430_H

#ifdef __cplusplus
extern "C" {
#include <cstdint>
#else
#include <stdint.h>
#endif /* __cplusplus */

#include "azure_c_shared_utility/tickcounter.h"

#define DEBUG_CLOCK 0

#ifndef TICK_T_DEFINED
#define TICK_T_DEFINED
typedef uint32_t tick_t;
#endif

#ifndef TICKHZ_T_DEFINED
#define TICKHZ_T_DEFINED
typedef uint32_t tickhz_t;
#endif

#ifndef TICKOVERFLOW_T_DEFINED
#define TICKOVERFLOW_T_DEFINED
typedef uint16_t tickoverflow_t;
#endif

extern
tickhz_t TIMER_A3_TICKS_PER_SEC;

#if DEBUG_CLOCK
extern
void
DEBUG_test_clock (
	uint32_t interval_ms_
);
#endif

extern
void
init_tick_timer_a3 (
	void
);

extern
tick_t
timer_a3_ticks (
	void
);

extern
tickoverflow_t
timer_a3_tick_overflows (
	void
);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CORE_MSP430_H */

