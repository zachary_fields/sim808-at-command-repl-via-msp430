// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <stdbool.h>

#include <driverlib.h>

#include "azure_c_shared_utility/core_msp430.h"
#include "azure_c_shared_utility/platform.h"
#include "azure_c_shared_utility/threadapi.h"
#include "azure_c_shared_utility/tlsio_sim808.h"

#define USE_SIM808_DTR 0
#define USE_SIM808_HW_FLOW_CONTROL 0

volatile bool _sim808_ready = false;


/******************************************************************************
 * Interrupt to signal SIM808 UART module has become available
 ******************************************************************************/
#pragma vector=PORT3_VECTOR
__interrupt void sim808Ready(void)
{
  #pragma diag_push
  /*
   * (ULP 10.1) ISR Sim808_Ready calls function GPIO_disableInterrupt.
   *
   * Recommend moving function call away from ISR, or inlining the function, or using pragmas
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1538
	(void)GPIO_disableInterrupt(GPIO_PORT_P3,GPIO_PIN5);
  #pragma diag_pop
	_sim808_ready = !_sim808_ready;
}


/******************************************************************************
 * Deinitialize the SIM808 chip
 ******************************************************************************/
void
deinitSim808 (
	void
) {
	_sim808_ready = true;

	/*
	 * The Port3 pin5 is connected to modem's status pin. It is used to monitor if modem is ON or OFF.
	 *  A HIGH on the pin indicates modem is ON otherwise OFF.
	 */
	(void)GPIO_setAsInputPin(GPIO_PORT_P3, GPIO_PIN5);

	/*
	 * Port4 pin6 is connected to GSM POWER KEY pin. A LOW pulse of
	 *   1 second will turn ON/OFF the Modem.
	 */
	(void)GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN6);
	(void)GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN6);

	/*
	 * The GSM status pin reflects the status of the SIM808
	 * If the device is powered down, the value is GPIO_INPUT_PIN_LOW.
	 * Otherwise if the device is powered up, the value is GPIO_INPUT_PIN_HIGH
	 */
	if (GPIO_getInputPinValue(GPIO_PORT_P3, GPIO_PIN5) == GPIO_INPUT_PIN_HIGH)
	{
		// Set interrupt to catch when the device is enabled
		(void)GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN5, GPIO_HIGH_TO_LOW_TRANSITION);
		(void)GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN5);

		// A one second pulse will toggle ON/OFF the modem
		(void)GPIO_setOutputHighOnPin(GPIO_PORT_P4, GPIO_PIN6);
		(void)ThreadAPI_Sleep(1100);
		(void)GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN6);
	} else {
		_sim808_ready = false;
	}

	return;
}


/******************************************************************************
 * Initialize the SIM808 chip
 ******************************************************************************/
void
initSim808 (
	void
) {
	_sim808_ready = false;

	/*
	 * The Port 3 Pin 5 is connected to the Sim808 Status pin.
	 *  A HIGH on the pin indicates modem is ON otherwise OFF.
	 */
	(void)GPIO_setAsInputPin(GPIO_PORT_P3, GPIO_PIN5);

	/*
	 * Port 3 Pin 6 is connected to the Sim808 Ready to Send pin.
	 * Port 3 Pin 7 is connected to the Sim808 Clear to Send pin.
	 */
  #if USE_SIM808_HW_FLOW_CONTROL
	(void)GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN6);
	(void)GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN6);
  #else
	(void)GPIO_setAsInputPin(GPIO_PORT_P3, GPIO_PIN6);
  #endif
	(void)GPIO_setAsInputPin(GPIO_PORT_P3, GPIO_PIN7);

	/*
	 * Port 4 pin 4 is connected to the Sim808 Ring Indicator pin.
	 */
	(void)GPIO_setAsInputPin(GPIO_PORT_P4, GPIO_PIN5);

	/*
	 * Port4 pin5 is connected to the GSM Data Terminal Ready pin.
	 */
  #if USE_SIM808_DTR
	(void)GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN5);
	(void)GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN5);
  #else
	(void)GPIO_setAsInputPin(GPIO_PORT_P4, GPIO_PIN5);
  #endif

	/*
	 * Port4 pin6 is connected to GSM POWER KEY pin. A LOW pulse of
	 * 1 second will turn ON/OFF the Modem.
	 */
	(void)GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN6);
	(void)GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN6);

	/*
	 * The GSM status pin reflects the status of the SIM808
	 * If the device is powered down, the value is GPIO_INPUT_PIN_LOW.
	 * Otherwise if the device is powered up, the value is GPIO_INPUT_PIN_HIGH
	 */
	if (GPIO_getInputPinValue(GPIO_PORT_P3, GPIO_PIN5) == GPIO_INPUT_PIN_LOW)
	{
		// Set interrupt to catch when the device is enabled
		(void)GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN5, GPIO_LOW_TO_HIGH_TRANSITION);
		(void)GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN5);

		(void)ThreadAPI_Sleep(550); // Device must be powered for 500ms before PWRKEY is pressed

		// A one second pulse will toggle ON/OFF the modem
		(void)GPIO_setOutputHighOnPin(GPIO_PORT_P4, GPIO_PIN6);
		(void)ThreadAPI_Sleep(1100);
		(void)GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN6);
	} else {
		_sim808_ready = true;
	}
}


int
platform_init (
	void
) {
	// Initialize the tick timer (used in millisecond timing and system delays)
	(void)init_tick_timer_a3();

	// Ensure the SMCLK is available to the UART module
	(void)CS_enableClockRequest(CS_SMCLK);

	// Wake the Sim808 via the PWRKEY
	(void)initSim808();

	// preform other MSP430 specific initializations here while SIM808 hardware is initializing...

  #pragma diag_push
  /*
   * (ULP 2.1) Detected SW delay loop using empty loop.
   *
   * Recommend using a timer module instead
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1527
	for(; !_sim808_ready ;);  // Wait for the SIM808 to wake
  #pragma diag_pop

	return 0;
}


void
platform_deinit (
	void
) {
	(void)deinitSim808();

	// preform other MSP430 specific deinitializations here while SIM808 hardware is deinitializing...
	(void)Timer_A_disableInterrupt(TIMER_A3_BASE);
	(void)Timer_A_stop(TIMER_A3_BASE);

	*((tick_t *)&TIMER_A3_TICKS_PER_SEC) = 0;

  #pragma diag_push
  /*
   * (ULP 2.1) Detected SW delay loop using empty loop.
   *
   * Recommend using a timer module instead
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1527
	for(; _sim808_ready ;);  // Wait for the SIM808 to sleep
  #pragma diag_pop

	return;
}


const IO_INTERFACE_DESCRIPTION *
platform_get_default_tlsio (
	void
) {
	return tlsio_sim808_get_interface_description();
}

