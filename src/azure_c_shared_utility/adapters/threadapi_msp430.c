// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <stdint.h>

#include "azure_c_shared_utility/core_msp430.h"
#include "azure_c_shared_utility/threadapi.h"

THREADAPI_RESULT tr = THREADAPI_OK;


THREADAPI_RESULT
ThreadAPI_Create (
	THREAD_HANDLE * threadHandle_,
	THREAD_START_FUNC func_,
	void * arg_
) {
	func_(arg_);
	return THREADAPI_OK;
}


THREADAPI_RESULT
ThreadAPI_Join (
	THREAD_HANDLE threadHandle_,
	int * res_
) {
	*res_ = tr;
	return THREADAPI_OK;
}


void
ThreadAPI_Exit (
	int res_
) {
	tr = (THREADAPI_RESULT)res_;
}


void
ThreadAPI_Sleep (
	unsigned int milliseconds_
) {
	tick_t start_time = timer_a3_ticks();

  #pragma diag_push
  /*
   * (ULP 2.1) Detected SW delay loop using empty loop.
   *
   * Recommend using a timer module instead
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1527
	// This implementation is imprecise as a result of clock divisions,
	// however it does guarantee AT LEAST the requested time will elapse.
	for (; ((TIMER_A3_TICKS_PER_SEC * milliseconds_) > ((timer_a3_ticks() - start_time) << 10)) ;);
  #pragma diag_pop
}

