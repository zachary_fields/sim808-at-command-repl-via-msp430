// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <stdint.h>

#include <driverlib.h>

#include "azure_c_shared_utility/core_msp430.h"

typedef struct counter_t {
	tick_t ticks;
	tickoverflow_t tick_overflows;
} counter_t;

typedef struct timerA80_t {
	tickoverflow_t counter_value;
	tick_t counter_overflows;
} timerA80_t;

typedef union tickcount_t {
	counter_t counter;
	timerA80_t timer_a;
} tickcount_t;

static tickcount_t system_ticks;

/*
 * The value of `CS_getSMCLK()` divided by the value
 * of the `clockSourceDivider` register of Timer A3
 */
tickhz_t TIMER_A3_TICKS_PER_SEC = 0;


#if DEBUG_CLOCK
#pragma diag_push
/*
 * (ULP 5.3) Detected printf() operation(s).
 *
 * Recommend moving them to RAM during run time or
 * not using as these are processing/power intensive
 */
//TODO: Confirm decision or address suppression diagnostic message
#pragma diag_suppress 1532
#include <stdio.h>

/*
 * This prints a visible clock to be used with a stopwatch
 */
void
DEBUG_test_clock (
	uint32_t interval_ms_
) {
	tick_t start_time = timer_a3_ticks(), test_time = 0;

	(void)printf("0         1         2         3         4         5         6\n");
	(void)printf("0123456789012345678901234567890123456789012345678901234567890\n");
	(void)printf(".");
    (void)fflush(stdout);

	for (;;) {
		test_time = timer_a3_ticks();
		if ( (TIMER_A3_TICKS_PER_SEC * interval_ms_) <= ((test_time - start_time) << 10) ) {
			start_time = test_time;
			(void)printf(".");
			(void)fflush(stdout);
		}
	}
}
#pragma diag_pop
#endif


/******************************************************************************
 * Interrupt to signal overflow of timer counter
 ******************************************************************************/
#pragma vector = TIMER3_A1_VECTOR
__interrupt void TIMER3_A1_ISR(void)
{
	switch (__even_in_range(TA3IV,14))
	{
      case TA3IV_NONE: break;
      case TA3IV_TACCR1: break;
      case TA3IV_3: break;
      case TA3IV_4: break;
      case TA3IV_5: break;
      case TA3IV_6: break;
      case TA3IV_TAIFG:
        ++system_ticks.timer_a.counter_overflows;
    	break;
      default: __never_executed();
	}
}


void
init_tick_timer_a3 (
	void
) {
	// Ensure the ACLK is available to the Timer A3 module
	(void)CS_enableClockRequest(CS_ACLK);

	// Update TIMER_A3_TICKS_PER_SEC with actual ticks per second
	// divided by the `.clockSourceDivider` value provided below
	TIMER_A3_TICKS_PER_SEC = (CS_getACLK() >> 4);  // "x >> 4" is equal to "x / 16"

	// Initialize the Timer A3
	Timer_A_initContinuousModeParam timer_a_continuous_mode_parameters = {
		.clockSource = TIMER_A_CLOCKSOURCE_ACLK,
		.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_16,
		.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_ENABLE,
		.timerClear = TIMER_A_SKIP_CLEAR,
		.startTimer = true,
	};
	(void)Timer_A_initContinuousMode(TIMER_A3_BASE, &timer_a_continuous_mode_parameters);

	// Initialize global variable
	system_ticks.counter.tick_overflows = 0;
	system_ticks.counter.ticks = 0;
}


tick_t
timer_a3_ticks (
	void
) {
	system_ticks.timer_a.counter_value = Timer_A_getCounterValue(TIMER_A3_BASE);

	return system_ticks.counter.ticks;
}


tickoverflow_t
timer_a3_tick_overflows (
	void
) {
	return system_ticks.counter.tick_overflows;
}
