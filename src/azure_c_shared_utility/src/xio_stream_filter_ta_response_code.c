// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_filter_ta_response_code.h"

#include <stdlib.h>

typedef struct XioStreamFilterTaResponseCodeState {
	bool last_char_was_carriage_return;
	size_t line_length;
	uint8_t ta_response_code;
	uint8_t ta_response_code_candidate;
} XioStreamFilterTaResponseCodeState;

#define TA_RESPONSE_CODE_INVALID '5'
#define TA_RESPONSE_CODE_LENGTH 3

void *
xio_stream_filter_ta_response_code_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
);

XIO_STREAM_FILTER_HANDLE
xio_stream_filter_ta_response_code_create (
	void
);

void
xio_stream_filter_ta_response_code_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
);

bool
xio_stream_filter_ta_response_code_full (
	XIO_STREAM_FILTER_HANDLE handle_
);

int
xio_stream_filter_ta_response_code_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

int
xio_stream_filter_ta_response_code_reset (
	XIO_STREAM_FILTER_HANDLE handle_
);

static
XioStreamFilterInterface xio_stream_filter_interface = {
	.xio_stream_filter_contents = xio_stream_filter_ta_response_code_contents,
	.xio_stream_filter_create = xio_stream_filter_ta_response_code_create,
	.xio_stream_filter_destroy = xio_stream_filter_ta_response_code_destroy,
	.xio_stream_filter_full = xio_stream_filter_ta_response_code_full,
	.xio_stream_filter_process_stream = xio_stream_filter_ta_response_code_process_stream,
	.xio_stream_filter_reset = xio_stream_filter_ta_response_code_reset
};


XioStreamFilterInterface *
xio_stream_filter_get_ta_response_code_interface (
	void
) {
	return &xio_stream_filter_interface;
}


inline
bool
isValidTaResponseCode (
	uint8_t ta_response_code_candidate_
) {
	return ((0x39 >= ta_response_code_candidate_) && (0x30 <= ta_response_code_candidate_) && (0x35 != ta_response_code_candidate_));
}


void *
xio_stream_filter_ta_response_code_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
) {
	XioStreamFilterTaResponseCodeState * ta_response_code_filter = handle_;
	if ( !handle_ ) { return NULL; }
	if ( content_length_ ) { *content_length_ = 1; }

	return &ta_response_code_filter->ta_response_code;
}


XIO_STREAM_FILTER_HANDLE
xio_stream_filter_ta_response_code_create (
	void
) {
	XioStreamFilterTaResponseCodeState * ta_response_code_filter = NULL;

	if ( NULL == (ta_response_code_filter = (XioStreamFilterTaResponseCodeState *)malloc(sizeof(XioStreamFilterTaResponseCodeState))) ) {
		//LogError("Unable to allocate memory for stream filter interface");
	} else if ( xio_stream_filter_ta_response_code_reset(ta_response_code_filter) ) {
		//LogError("Failed to initialize values for ta response code stream filter interface");
	} else {
		// Creation successful
	}

	return (XIO_STREAM_FILTER_HANDLE)ta_response_code_filter;
}


void
xio_stream_filter_ta_response_code_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	free(handle_);
}


bool
xio_stream_filter_ta_response_code_full (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTaResponseCodeState * ta_response_code_filter = handle_;
	if ( !handle_ ) { return false; }

	return isValidTaResponseCode(ta_response_code_filter->ta_response_code);
}


int
xio_stream_filter_ta_response_code_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamFilterTaResponseCodeState * ta_response_code_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else if ( isValidTaResponseCode(ta_response_code_filter->ta_response_code) ) {
		// Match has occured, no further processing required
	} else {
		bool is_valid_ta_response_code = false;

		// Scan stream for TA response code
		for (; io_stream_length_ > 0 && !is_valid_ta_response_code ; --io_stream_length_, ++io_stream_) {
			bool is_end_of_line = false;
			bool is_ta_response_code_length = false;

			++ta_response_code_filter->line_length;

			// Save TA response code candidate
			ta_response_code_filter->ta_response_code_candidate = (
				((1 == ta_response_code_filter->line_length) * (*io_stream_)) +
				((1 != ta_response_code_filter->line_length) * ta_response_code_filter->ta_response_code_candidate)
			);

			// Test for end of line condition
			is_end_of_line = (ta_response_code_filter->last_char_was_carriage_return && ('\n' == *io_stream_));
			ta_response_code_filter->last_char_was_carriage_return = ('\r' == *io_stream_);

			// Test for correct length
			is_ta_response_code_length = (TA_RESPONSE_CODE_LENGTH == ta_response_code_filter->line_length);
			ta_response_code_filter->line_length = (!is_end_of_line * ta_response_code_filter->line_length);

			// Test all conditions for valid ta response code
			is_valid_ta_response_code = (is_end_of_line && is_ta_response_code_length && isValidTaResponseCode(ta_response_code_filter->ta_response_code_candidate));
			ta_response_code_filter->ta_response_code = (
				(is_valid_ta_response_code * ta_response_code_filter->ta_response_code_candidate) +
				(!is_valid_ta_response_code * ta_response_code_filter->ta_response_code)
			);
		}
	}

	return error;
}


int
xio_stream_filter_ta_response_code_reset (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTaResponseCodeState * ta_response_code_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else {
		ta_response_code_filter->last_char_was_carriage_return = false;
		ta_response_code_filter->line_length = 0;
		ta_response_code_filter->ta_response_code = TA_RESPONSE_CODE_INVALID;
		ta_response_code_filter->ta_response_code_candidate = TA_RESPONSE_CODE_INVALID;
	}

	return error;
}

