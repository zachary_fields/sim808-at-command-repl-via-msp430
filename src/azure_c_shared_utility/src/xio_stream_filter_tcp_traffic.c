// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_filter_tcp_traffic.h"

#include <stdlib.h>

#include "azure_c_shared_utility/vector.h"

typedef struct XioStreamFilterTcpTrafficState {
	VECTOR_HANDLE tcp_traffic;
	size_t state_machine_index;
	bool transmission_complete;
} XioStreamFilterTcpTrafficState;

void *
xio_stream_filter_tcp_traffic_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
);

XIO_STREAM_FILTER_HANDLE
xio_stream_filter_tcp_traffic_create (
	void
);

void
xio_stream_filter_tcp_traffic_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
);

bool
xio_stream_filter_tcp_traffic_full (
	XIO_STREAM_FILTER_HANDLE handle_
);

int
xio_stream_filter_tcp_traffic_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

int
xio_stream_filter_tcp_traffic_reset (
	XIO_STREAM_FILTER_HANDLE handle_
);

static
XioStreamFilterInterface xio_stream_filter_interface = {
	.xio_stream_filter_contents = xio_stream_filter_tcp_traffic_contents,
	.xio_stream_filter_create = xio_stream_filter_tcp_traffic_create,
	.xio_stream_filter_destroy = xio_stream_filter_tcp_traffic_destroy,
	.xio_stream_filter_full = xio_stream_filter_tcp_traffic_full,
	.xio_stream_filter_process_stream = xio_stream_filter_tcp_traffic_process_stream,
	.xio_stream_filter_reset = xio_stream_filter_tcp_traffic_reset
};


XioStreamFilterInterface *
xio_stream_filter_get_tcp_traffic_interface (
	void
) {
	return &xio_stream_filter_interface;
}


void *
xio_stream_filter_tcp_traffic_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
) {
	XioStreamFilterTcpTrafficState * tcp_traffic_filter = handle_;

	if ( !handle_ ) { return NULL; }
	if ( content_length_ ) { *content_length_ = VECTOR_size(tcp_traffic_filter->tcp_traffic); }
	(void)VECTOR_clear(tcp_traffic_filter->tcp_traffic);

	return VECTOR_front(tcp_traffic_filter->tcp_traffic);
}


XIO_STREAM_FILTER_HANDLE
xio_stream_filter_tcp_traffic_create (
	void
) {
	XioStreamFilterTcpTrafficState * tcp_traffic_filter = NULL;

	if ( NULL == (tcp_traffic_filter = (XioStreamFilterTcpTrafficState *)malloc(sizeof(XioStreamFilterTcpTrafficState))) ) {
		//LogError("Unable to allocate memory for stream filter interface");
	} else if ( NULL == (tcp_traffic_filter->tcp_traffic = VECTOR_create(sizeof(uint8_t))) ) {
		(void)free(tcp_traffic_filter);
		//LogError("Failed to initialize buffer for TCP traffic filter");
	} else if ( xio_stream_filter_tcp_traffic_reset(tcp_traffic_filter) ) {
		(void)VECTOR_destroy(tcp_traffic_filter->tcp_traffic);
		(void)free(tcp_traffic_filter);
		//LogError("Failed to initialize values for TCP traffic stream filter interface");
	} else {
		//LogInfo("TCP traffic filter created successfully");
	}

	return (XIO_STREAM_FILTER_HANDLE)tcp_traffic_filter;
}


void
xio_stream_filter_tcp_traffic_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTcpTrafficState * tcp_traffic_filter = handle_;

	if ( handle_ ) {
		(void)VECTOR_destroy(tcp_traffic_filter->tcp_traffic);
		(void)free(handle_);
	}
}


bool
xio_stream_filter_tcp_traffic_full (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTcpTrafficState * tcp_traffic_filter = handle_;
	if ( !handle_ ) { return false; }

	return tcp_traffic_filter->transmission_complete;
}


int
xio_stream_filter_tcp_traffic_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamFilterTcpTrafficState * tcp_traffic_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else if ( tcp_traffic_filter->transmission_complete ) {
		// Transmission complete, no further processing required
	} else {
		for (; io_stream_length_ > 0 && !tcp_traffic_filter->transmission_complete ; --io_stream_length_, ++io_stream_) {
			switch (tcp_traffic_filter->state_machine_index) {
			  case 0:
				tcp_traffic_filter->state_machine_index = ('S' == *io_stream_);
				break;
			  case 1:
				tcp_traffic_filter->state_machine_index = (
					(('E' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 2:
				tcp_traffic_filter->state_machine_index = (
					(('N' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 3:
				tcp_traffic_filter->state_machine_index = (
					(('D' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 4:
				tcp_traffic_filter->state_machine_index = (
					((' ' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 5:
				tcp_traffic_filter->state_machine_index = (
					(('O' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					(('F' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 11)) +
					('S' == *io_stream_)
				);
				break;
			  case 6:
				tcp_traffic_filter->state_machine_index = (
					(('K' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 7:
				tcp_traffic_filter->state_machine_index = (
					(('\r' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 8:
				tcp_traffic_filter->state_machine_index = (
					(('\n' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 9:
				if ( '\r' == *io_stream_ ) {
					++tcp_traffic_filter->state_machine_index;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
				}
				break;
			  case 10:
				if ( '\n' == *io_stream_ ) {
					++tcp_traffic_filter->state_machine_index;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "\r", 1);
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
					tcp_traffic_filter->state_machine_index = 9;
				}
				break;
			  case 11:
				if ( '0' == *io_stream_ ) {
					++tcp_traffic_filter->state_machine_index;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "\r\n", 2);
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
					tcp_traffic_filter->state_machine_index = 9;
				}
				break;
			  case 12:
				if ( '\r' == *io_stream_ ) {
					++tcp_traffic_filter->state_machine_index;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "\r\n0", 3);
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
					tcp_traffic_filter->state_machine_index = 9;
				}
				break;
			  case 13:
				if ( '\n' == *io_stream_ ) {
					++tcp_traffic_filter->state_machine_index;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "\r\n0\r", 4);
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
					tcp_traffic_filter->state_machine_index = 9;
				}
				break;
			  case 14:
				if ( '\r' == *io_stream_ ) {
					++tcp_traffic_filter->state_machine_index;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "\r\n0\r\n", 5);
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
					tcp_traffic_filter->state_machine_index = 9;
				}
				break;
			  case 15:
				tcp_traffic_filter->transmission_complete = ('\n' == *io_stream_);
				if ( tcp_traffic_filter->transmission_complete ) {
					tcp_traffic_filter->state_machine_index = 0;
				} else {
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "\r\n0\r\n\r", 6);
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, io_stream_, 1);
					tcp_traffic_filter->state_machine_index = 9;
				}
				break;
			  case 16:
				tcp_traffic_filter->state_machine_index = (
					(('A' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 17:
				tcp_traffic_filter->state_machine_index = (
					(('I' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 18:
				tcp_traffic_filter->state_machine_index = (
					(('L' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 19:
				tcp_traffic_filter->state_machine_index = (
					(('\r' == *io_stream_) * (tcp_traffic_filter->state_machine_index + 1)) +
					('S' == *io_stream_)
				);
				break;
			  case 20:
				tcp_traffic_filter->transmission_complete = ('\n' == *io_stream_);
				if ( tcp_traffic_filter->transmission_complete ) {
					tcp_traffic_filter->state_machine_index = 0;
					VECTOR_push_back(tcp_traffic_filter->tcp_traffic, "FAIL", 4);
				} else {
					tcp_traffic_filter->state_machine_index = ('S' == *io_stream_);
				}
				break;
			  default: break;
			}
		}
	}

	return error;
}


int
xio_stream_filter_tcp_traffic_reset (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTcpTrafficState * tcp_traffic_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else {
		(void)VECTOR_clear(tcp_traffic_filter->tcp_traffic);
		tcp_traffic_filter->state_machine_index = 0;
		tcp_traffic_filter->transmission_complete = false;
	}

	return error;
}

