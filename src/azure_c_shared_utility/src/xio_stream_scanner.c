// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_scanner.h"

#include <string.h>

#include "azure_c_shared_utility/xlogging.h"

typedef struct XioStreamScannerState {
	uint8_t * query;
	size_t query_index;
	size_t query_length;
} XioStreamScannerState;


XIO_STREAM_SCANNER_HANDLE
xio_stream_scanner_create (
	void
) {
	XioStreamScannerState * stream_scanner;

	if ( NULL == (stream_scanner = (XioStreamScannerState *)calloc(sizeof(XioStreamScannerState), 1)) ) {
		LogError("Unable to allocate memory for XIO stream scanner");
	} else {
		LogInfo("Stream scanner successfully initialized");
	}

	return (XIO_STREAM_SCANNER_HANDLE)stream_scanner;
}


void
xio_stream_scanner_destroy (
	XIO_STREAM_SCANNER_HANDLE handle_
) {
	XioStreamScannerState * stream_scanner = handle_;

	if ( NULL == handle_ ) {
		LogError("Invalid handle passed to XIO stream scanner destroy");
	} else {
		(void)free(stream_scanner->query);
		(void)free(stream_scanner);
	}
}


int
xio_stream_scanner_init (
	XIO_STREAM_SCANNER_HANDLE handle_,
	const uint8_t * query_,
	const size_t query_length_
) {
	XioStreamScannerState * stream_scanner = handle_;
	int error;

	if ( NULL == handle_ ) {
		error = __LINE__;
	} else if ( NULL == query_ ) {
		error = __LINE__;
	} else if ( NULL == (stream_scanner->query = (uint8_t *)realloc(stream_scanner->query, query_length_ + 1)) ) {
		error = __LINE__;
	} else if ( NULL == memcpy(stream_scanner->query, query_, query_length_) ) {
		error = __LINE__;
	} else {
		error = 0;
		stream_scanner->query[query_length_] = '\0';
		stream_scanner->query_index = 0;
		stream_scanner->query_length = query_length_;
	}

	return error;
}


bool
xio_stream_scanner_matched (
	XIO_STREAM_SCANNER_HANDLE handle_
) {
	XioStreamScannerState * stream_scanner = handle_;
	bool matched;

	if ( NULL == handle_ ) {
		LogError("Invalid handle passed to XIO stream scanner destroy");
		matched = false;
	} else {
		matched = (stream_scanner->query_length == stream_scanner->query_index);
	}

	return matched;
}


int
xio_stream_scanner_process_stream (
	XIO_STREAM_SCANNER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamScannerState * stream_scanner = handle_;
	int error;

	if ( NULL == handle_ ) {
		error = __LINE__;
	} else if ( stream_scanner->query_length == stream_scanner->query_index ) {
		// Match has occured, no further processing required
		error = 0;
	} else {
		error = 0;
		for (; (stream_scanner->query_length != stream_scanner->query_index) && (io_stream_length_ > 0) ; --io_stream_length_, ++io_stream_) {
			stream_scanner->query_index = ((stream_scanner->query[stream_scanner->query_index] == *io_stream_) * (stream_scanner->query_index + 1));
			stream_scanner->query_index = (
				((0 == stream_scanner->query_index) * (stream_scanner->query[0] == *io_stream_)) +
				((0 != stream_scanner->query_index) * stream_scanner->query_index)
			);
		}
	}

	return error;
}

