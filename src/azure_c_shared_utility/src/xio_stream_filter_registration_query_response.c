// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_filter_registration_query_response.h"

#include <stdlib.h>

typedef struct XioStreamFilterRegistrationQueryResponseState {
	RegistrationQueryResponse registration_query_response;
	size_t state_machine_index;
	bool valid_registration_query_response;
} XioStreamFilterRegistrationQueryResponseState;

void *
xio_stream_filter_registration_query_response_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
);

XIO_STREAM_FILTER_HANDLE
xio_stream_filter_registration_query_response_create (
	void
);

void
xio_stream_filter_registration_query_response_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
);

bool
xio_stream_filter_registration_query_response_full (
	XIO_STREAM_FILTER_HANDLE handle_
);

int
xio_stream_filter_registration_query_response_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

int
xio_stream_filter_registration_query_response_reset (
	XIO_STREAM_FILTER_HANDLE handle_
);

static
XioStreamFilterInterface xio_stream_filter_interface = {
	.xio_stream_filter_contents = xio_stream_filter_registration_query_response_contents,
	.xio_stream_filter_create = xio_stream_filter_registration_query_response_create,
	.xio_stream_filter_destroy = xio_stream_filter_registration_query_response_destroy,
	.xio_stream_filter_full = xio_stream_filter_registration_query_response_full,
	.xio_stream_filter_process_stream = xio_stream_filter_registration_query_response_process_stream,
	.xio_stream_filter_reset = xio_stream_filter_registration_query_response_reset
};


XioStreamFilterInterface *
xio_stream_filter_get_registration_query_response_interface (
	void
) {
	return &xio_stream_filter_interface;
}


void *
xio_stream_filter_registration_query_response_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
) {
	XioStreamFilterRegistrationQueryResponseState * registration_query_response_filter = handle_;
	if ( !handle_ ) { return NULL; }
	if ( content_length_ ) { *content_length_ = sizeof(RegistrationQueryResponse); }

	return &registration_query_response_filter->registration_query_response;
}


XIO_STREAM_FILTER_HANDLE
xio_stream_filter_registration_query_response_create (
	void
) {
	XioStreamFilterRegistrationQueryResponseState * registration_query_response_filter = NULL;

	if ( NULL == (registration_query_response_filter = (XioStreamFilterRegistrationQueryResponseState *)malloc(sizeof(XioStreamFilterRegistrationQueryResponseState))) ) {
		//LogError("Unable to allocate memory for stream filter interface");
	} else if ( xio_stream_filter_registration_query_response_reset(registration_query_response_filter) ) {
		//LogError("Failed to initalize values for query response stream filter interface");
	} else {
		// Creation successful
	}

	return (XIO_STREAM_FILTER_HANDLE)registration_query_response_filter;
}


void
xio_stream_filter_registration_query_response_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	(void)free(handle_);
}


bool
xio_stream_filter_registration_query_response_full (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterRegistrationQueryResponseState * registration_query_response_filter = handle_;
	if ( !handle_ ) { return false; }

	return registration_query_response_filter->valid_registration_query_response;
}


int
xio_stream_filter_registration_query_response_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamFilterRegistrationQueryResponseState * registration_query_response_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else if ( registration_query_response_filter->valid_registration_query_response ) {
		// Match has occured, no further processing required
	} else {
		for (; io_stream_length_ > 0 && !registration_query_response_filter->valid_registration_query_response ; --io_stream_length_, ++io_stream_) {
			switch (registration_query_response_filter->state_machine_index) {
			  case 0:
				registration_query_response_filter->state_machine_index = ('+' == *io_stream_);
				break;
			  case 1:
				registration_query_response_filter->state_machine_index = (
					(('C' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 2:
				registration_query_response_filter->state_machine_index = (
					(('G' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					(('R' == *io_stream_) * (registration_query_response_filter->state_machine_index + 2)) +
					('+' == *io_stream_)
				);
				break;
			  case 3:
				registration_query_response_filter->state_machine_index = (
					(('R' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 4:
				registration_query_response_filter->state_machine_index = (
					(('E' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 5:
				registration_query_response_filter->state_machine_index = (
					(('G' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 6:
				registration_query_response_filter->state_machine_index = (
					((':' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 7:
				registration_query_response_filter->state_machine_index = (
					((' ' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 8:
				// [0-2]
				registration_query_response_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x32 >= *io_stream_)) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.n = *io_stream_;
				break;
			  case 9:
				registration_query_response_filter->state_machine_index = (
					((',' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 10:
				// [0-5]
				registration_query_response_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.stat = *io_stream_;
				break;
			  case 11:
				registration_query_response_filter->state_machine_index = (
					((',' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					(('\r' == *io_stream_) * (registration_query_response_filter->state_machine_index + 15)) +
					('+' == *io_stream_)
				);
				break;
			  case 12:
				registration_query_response_filter->state_machine_index = (
					(('\"' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 13:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.lac[0] = *io_stream_;
				break;
			  case 14:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.lac[1] = *io_stream_;
				break;
			  case 15:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.lac[2] = *io_stream_;
				break;
			  case 16:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.lac[3] = *io_stream_;
				break;
			  case 17:
				registration_query_response_filter->state_machine_index = (
					(('\"' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 18:
				registration_query_response_filter->state_machine_index = (
					((',' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 19:
				registration_query_response_filter->state_machine_index = (
					(('\"' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 20:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.ci[0] = *io_stream_;
				break;
			  case 21:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.ci[1] = *io_stream_;
				break;
			  case 22:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.ci[2] = *io_stream_;
				break;
			  case 23:
				// ([0-9]|[A-F]|[a-f])
				registration_query_response_filter->state_machine_index = (
					((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || ((0x41 <= *io_stream_) && (0x46 >= *io_stream_)) || ((0x61 <= *io_stream_) && (0x66 >= *io_stream_))) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				registration_query_response_filter->registration_query_response.ci[3] = *io_stream_;
				break;
			  case 24:
				registration_query_response_filter->state_machine_index = (
					(('\"' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 25:
				registration_query_response_filter->state_machine_index = (
					(('\r' == *io_stream_) * (registration_query_response_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 26:
			    registration_query_response_filter->valid_registration_query_response = ('\n' == *io_stream_);
				registration_query_response_filter->state_machine_index = ('+' == *io_stream_);
				break;
			  default: break;
			}
		}
	}

	return error;
}


int
xio_stream_filter_registration_query_response_reset (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterRegistrationQueryResponseState * registration_query_response_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else {
		registration_query_response_filter->registration_query_response.n = 0;
		registration_query_response_filter->registration_query_response.stat = 0;
		registration_query_response_filter->registration_query_response.lac[0] = '\0';
		registration_query_response_filter->registration_query_response.lac[1] = '\0';
		registration_query_response_filter->registration_query_response.lac[2] = '\0';
		registration_query_response_filter->registration_query_response.lac[3] = '\0';
		registration_query_response_filter->registration_query_response.lac[4] = '\0';
		registration_query_response_filter->registration_query_response.ci[0] = '\0';
		registration_query_response_filter->registration_query_response.ci[1] = '\0';
		registration_query_response_filter->registration_query_response.ci[2] = '\0';
		registration_query_response_filter->registration_query_response.ci[3] = '\0';
		registration_query_response_filter->registration_query_response.ci[4] = '\0';
		registration_query_response_filter->state_machine_index = 0;
		registration_query_response_filter->valid_registration_query_response = false;
	}

	return error;
}

