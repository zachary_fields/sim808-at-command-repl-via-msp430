// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_filter.h"

#include <stdlib.h>

typedef struct XioStreamFilterState {
	XioStreamFilterInterface * xio_stream_filter_interface;
	XIO_STREAM_FILTER_HANDLE xio_stream_filter_instance;
} XioStreamFilterState;


void *
xio_stream_filter_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
) {
	XioStreamFilterState * filter = (XioStreamFilterState *)handle_;
	if ( !handle_ ) { return NULL; }

	return filter->xio_stream_filter_interface->xio_stream_filter_contents(filter->xio_stream_filter_instance, content_length_);
}


XIO_STREAM_FILTER_HANDLE
xio_stream_filter_create (
	XioStreamFilterInterface * xio_stream_filter_interface_
) {
	XioStreamFilterState * filter = NULL;

	if ( NULL == (filter = (XioStreamFilterState *)malloc(sizeof(XioStreamFilterState))) ) {
		//LogError("Unable to allocate memory for stream filter interface");
	} else if ( NULL == (filter->xio_stream_filter_instance = xio_stream_filter_interface_->xio_stream_filter_create()) ) {
		free(filter);
		filter = NULL;
		//LogError("Unable to allocate memory for stream filter instance");
	} else {
		filter->xio_stream_filter_interface = xio_stream_filter_interface_;
	}

	return (XIO_STREAM_FILTER_HANDLE)filter;
}


void
xio_stream_filter_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterState * filter = (XioStreamFilterState *)handle_;
	if ( !handle_ ) { return; }

	(void)filter->xio_stream_filter_interface->xio_stream_filter_destroy(filter->xio_stream_filter_instance);
}


bool
xio_stream_filter_full (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterState * filter = (XioStreamFilterState *)handle_;
	if ( !handle_ ) { return NULL; }

	return filter->xio_stream_filter_interface->xio_stream_filter_full(filter->xio_stream_filter_instance);
}


int
xio_stream_filter_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamFilterState * filter = (XioStreamFilterState *)handle_;
	if ( !handle_ ) { return NULL; }

	return filter->xio_stream_filter_interface->xio_stream_filter_process_stream(filter->xio_stream_filter_instance, io_stream_, io_stream_length_);
}


int
xio_stream_filter_reset (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterState * filter = (XioStreamFilterState *)handle_;
	if ( !handle_ ) { return NULL; }

	return filter->xio_stream_filter_interface->xio_stream_filter_reset(filter->xio_stream_filter_instance);
}

