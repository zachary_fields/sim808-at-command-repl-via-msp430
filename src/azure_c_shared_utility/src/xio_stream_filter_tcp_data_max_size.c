// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_filter_tcp_data_max_size.h"

#include <stdlib.h>

typedef struct XioStreamFilterTcpDataMaxSizeState {
	size_t tcp_data_max_size;
	size_t state_machine_index;
	bool valid_tcp_data_max_size;
} XioStreamFilterTcpDataMaxSizeState;

void *
xio_stream_filter_tcp_data_max_size_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
);

XIO_STREAM_FILTER_HANDLE
xio_stream_filter_tcp_data_max_size_create (
	void
);

void
xio_stream_filter_tcp_data_max_size_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
);

bool
xio_stream_filter_tcp_data_max_size_full (
	XIO_STREAM_FILTER_HANDLE handle_
);

int
xio_stream_filter_tcp_data_max_size_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

int
xio_stream_filter_tcp_data_max_size_reset (
	XIO_STREAM_FILTER_HANDLE handle_
);

static
XioStreamFilterInterface xio_stream_filter_interface = {
	.xio_stream_filter_contents = xio_stream_filter_tcp_data_max_size_contents,
	.xio_stream_filter_create = xio_stream_filter_tcp_data_max_size_create,
	.xio_stream_filter_destroy = xio_stream_filter_tcp_data_max_size_destroy,
	.xio_stream_filter_full = xio_stream_filter_tcp_data_max_size_full,
	.xio_stream_filter_process_stream = xio_stream_filter_tcp_data_max_size_process_stream,
	.xio_stream_filter_reset = xio_stream_filter_tcp_data_max_size_reset
};


XioStreamFilterInterface *
xio_stream_filter_get_tcp_data_max_size_interface (
	void
) {
	return &xio_stream_filter_interface;
}


void *
xio_stream_filter_tcp_data_max_size_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
) {
	XioStreamFilterTcpDataMaxSizeState * tcp_data_max_size_filter = handle_;
	if ( !handle_ ) { return NULL; }
	if ( content_length_ ) { *content_length_ = sizeof(size_t); }

	return &tcp_data_max_size_filter->tcp_data_max_size;
}


XIO_STREAM_FILTER_HANDLE
xio_stream_filter_tcp_data_max_size_create (
	void
) {
	XioStreamFilterTcpDataMaxSizeState * tcp_data_max_size_filter = NULL;

	if ( NULL == (tcp_data_max_size_filter = (XioStreamFilterTcpDataMaxSizeState *)malloc(sizeof(XioStreamFilterTcpDataMaxSizeState))) ) {
		//LogError("Unable to allocate memory for stream filter interface");
	} else if ( xio_stream_filter_tcp_data_max_size_reset(tcp_data_max_size_filter) ) {
		//LogError("Failed to initalize values for tcp data max size stream filter interface");
	} else {
		// Creation successful
	}

	return (XIO_STREAM_FILTER_HANDLE)tcp_data_max_size_filter;
}


void
xio_stream_filter_tcp_data_max_size_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	(void)free(handle_);
}


bool
xio_stream_filter_tcp_data_max_size_full (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTcpDataMaxSizeState * tcp_data_max_size_filter = handle_;
	if ( !handle_ ) { return false; }

	return tcp_data_max_size_filter->valid_tcp_data_max_size;
}


int
xio_stream_filter_tcp_data_max_size_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamFilterTcpDataMaxSizeState * tcp_data_max_size_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else if ( tcp_data_max_size_filter->valid_tcp_data_max_size ) {
		// Match has occured, no further processing required
	} else {
		for (; io_stream_length_ > 0 && !tcp_data_max_size_filter->valid_tcp_data_max_size ; --io_stream_length_, ++io_stream_) {
			switch (tcp_data_max_size_filter->state_machine_index) {
			  case 0:
				tcp_data_max_size_filter->state_machine_index = ('+' == *io_stream_);
				break;
			  case 1:
				tcp_data_max_size_filter->state_machine_index = (
					(('C' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 2:
				tcp_data_max_size_filter->state_machine_index = (
					(('I' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 3:
				tcp_data_max_size_filter->state_machine_index = (
					(('P' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 4:
				tcp_data_max_size_filter->state_machine_index = (
					(('S' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 5:
				tcp_data_max_size_filter->state_machine_index = (
					(('E' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 6:
				tcp_data_max_size_filter->state_machine_index = (
					(('N' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 7:
				tcp_data_max_size_filter->state_machine_index = (
					(('D' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 8:
				tcp_data_max_size_filter->state_machine_index = (
					((':' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				break;
			  case 9:
				tcp_data_max_size_filter->state_machine_index = (
					((' ' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				tcp_data_max_size_filter->tcp_data_max_size = 0;
				break;
			  case 10:
				// [0-9]
				tcp_data_max_size_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * tcp_data_max_size_filter->state_machine_index) +
					(('\r' == *io_stream_) * (tcp_data_max_size_filter->state_machine_index + 1)) +
					('+' == *io_stream_)
				);
				tcp_data_max_size_filter->tcp_data_max_size = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * ((tcp_data_max_size_filter->tcp_data_max_size * 10) + (*io_stream_ - 0x30))) +
					(('\r' == *io_stream_) * tcp_data_max_size_filter->tcp_data_max_size)
				);
				break;
			  case 11:
			    tcp_data_max_size_filter->valid_tcp_data_max_size = ('\n' == *io_stream_);
				tcp_data_max_size_filter->state_machine_index = ('+' == *io_stream_);
				break;
			  default: break;
			}
		}
	}

	return error;
}


int
xio_stream_filter_tcp_data_max_size_reset (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterTcpDataMaxSizeState * tcp_data_max_size_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else {
		tcp_data_max_size_filter->tcp_data_max_size = 0;
		tcp_data_max_size_filter->state_machine_index = 0;
		tcp_data_max_size_filter->valid_tcp_data_max_size = false;
	}

	return error;
}

