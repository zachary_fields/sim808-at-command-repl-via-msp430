// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/xio_stream_filter_ip_address.h"

#include <string.h>

typedef struct XioStreamFilterIpAddressState {
	uint8_t ip_address[16];
	size_t ip_address_index;
	size_t state_machine_index;
	bool valid_ip_address;
} XioStreamFilterIpAddressState;

void *
xio_stream_filter_ip_address_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
);

XIO_STREAM_FILTER_HANDLE
xio_stream_filter_ip_address_create (
	void
);

void
xio_stream_filter_ip_address_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
);

bool
xio_stream_filter_ip_address_full (
	XIO_STREAM_FILTER_HANDLE handle_
);

int
xio_stream_filter_ip_address_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

int
xio_stream_filter_ip_address_reset (
	XIO_STREAM_FILTER_HANDLE handle_
);

static
XioStreamFilterInterface xio_stream_filter_interface = {
	.xio_stream_filter_contents = xio_stream_filter_ip_address_contents,
	.xio_stream_filter_create = xio_stream_filter_ip_address_create,
	.xio_stream_filter_destroy = xio_stream_filter_ip_address_destroy,
	.xio_stream_filter_full = xio_stream_filter_ip_address_full,
	.xio_stream_filter_process_stream = xio_stream_filter_ip_address_process_stream,
	.xio_stream_filter_reset = xio_stream_filter_ip_address_reset
};


XioStreamFilterInterface *
xio_stream_filter_get_ip_address_interface (
	void
) {
	return &xio_stream_filter_interface;
}


void *
xio_stream_filter_ip_address_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
) {
	XioStreamFilterIpAddressState * ip_address_filter = handle_;
	if ( !handle_ ) { return NULL; }
	if ( content_length_ ) { *content_length_ = strlen((const char *)ip_address_filter->ip_address); }

	return ip_address_filter->ip_address;
}


XIO_STREAM_FILTER_HANDLE
xio_stream_filter_ip_address_create (
	void
) {
	XioStreamFilterIpAddressState * ip_address_filter = NULL;

	if ( NULL == (ip_address_filter = (XioStreamFilterIpAddressState *)malloc(sizeof(XioStreamFilterIpAddressState))) ) {
		//LogError("Unable to allocate memory for stream filter interface");
	} else if ( xio_stream_filter_ip_address_reset(ip_address_filter) ) {
		//LogError("Failed to initalize values for ip address stream filter interface");
	} else {
		// Creation successful
	}

	return (XIO_STREAM_FILTER_HANDLE)ip_address_filter;
}


void
xio_stream_filter_ip_address_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	(void)free(handle_);
}


bool
xio_stream_filter_ip_address_full (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterIpAddressState * ip_address_filter = handle_;
	if ( !handle_ ) { return false; }

	return ip_address_filter->valid_ip_address;
}


int
xio_stream_filter_ip_address_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
) {
	XioStreamFilterIpAddressState * ip_address_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else if ( ip_address_filter->valid_ip_address ) {
		// Match has occured, no further processing required
	} else {
		for (; io_stream_length_ > 0 && !ip_address_filter->valid_ip_address ; --io_stream_length_, ++io_stream_) {
			switch (ip_address_filter->state_machine_index) {
			  case 0:
				ip_address_filter->state_machine_index = ('\r' == *io_stream_);
				break;
			  case 1:
				ip_address_filter->state_machine_index = (
					(('\n' == *io_stream_) * (ip_address_filter->state_machine_index + 1))
				  + ('\r' == *io_stream_)
				);
				break;
			  case 2:
				ip_address_filter->state_machine_index = (
					(('0' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					(('1' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					(('2' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x33 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 3:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 4:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x34 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('5' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x36 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 4)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 5:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 6:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 7:
				ip_address_filter->state_machine_index = (
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (('.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 8:
				ip_address_filter->state_machine_index = (
					(('0' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					(('1' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					(('2' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x33 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 9:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 10:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x34 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('5' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x36 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 4)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 11:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 12:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 13:
				ip_address_filter->state_machine_index = (
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (('.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 14:
				ip_address_filter->state_machine_index = (
					(('0' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					(('1' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					(('2' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x33 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 15:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 16:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x34 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('5' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x36 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 4)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 17:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 18:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = ((((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) || '.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 19:
				ip_address_filter->state_machine_index = (
					(('.' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (('.' == *io_stream_) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 20:
				ip_address_filter->state_machine_index = (
					(('0' == *io_stream_) * (ip_address_filter->state_machine_index + 5)) +
					(('1' == *io_stream_) * (ip_address_filter->state_machine_index + 1)) +
					(('2' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x33 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					('\r' == *io_stream_)
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1));
				break;
			  case 21:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('\r' == *io_stream_) * (ip_address_filter->state_machine_index + 5))
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1)) +
					(('\r' == *io_stream_) * ip_address_filter->ip_address_index)
				);
				break;
			  case 22:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x34 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('5' == *io_stream_) * (ip_address_filter->state_machine_index + 2)) +
					(((0x36 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 3)) +
					(('\r' == *io_stream_) * (ip_address_filter->state_machine_index + 4))
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1)) +
					(('\r' == *io_stream_) * ip_address_filter->ip_address_index)
				);
				break;
			  case 23:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->state_machine_index + 2)) +
					(('\r' == *io_stream_) * (ip_address_filter->state_machine_index + 3))
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1)) +
					(('\r' == *io_stream_) * ip_address_filter->ip_address_index)
				);
				break;
			  case 24:
				ip_address_filter->state_machine_index = (
					(((0x30 <= *io_stream_) && (0x35 >= *io_stream_)) * (ip_address_filter->state_machine_index + 1)) +
					(('\r' == *io_stream_) * (ip_address_filter->state_machine_index + 2))
				);
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = *io_stream_;
				ip_address_filter->ip_address_index = (
					(((0x30 <= *io_stream_) && (0x39 >= *io_stream_)) * (ip_address_filter->ip_address_index + 1)) +
					(('\r' == *io_stream_) * ip_address_filter->ip_address_index)
				);
				break;
			  case 25:
				ip_address_filter->state_machine_index = (('\r' == *io_stream_) * (ip_address_filter->state_machine_index + 1));
				ip_address_filter->ip_address[ip_address_filter->ip_address_index] = '\0';
				ip_address_filter->ip_address_index = (('\r' == *io_stream_) * ip_address_filter->ip_address_index);
				break;
			  case 26:
				ip_address_filter->valid_ip_address = ('\n' == *io_stream_);
				ip_address_filter->state_machine_index = ('\r' == *io_stream_);
				ip_address_filter->ip_address_index = 0;
				break;
			  default: break;
			}
		}
	}

	return error;
}


int
xio_stream_filter_ip_address_reset (
	XIO_STREAM_FILTER_HANDLE handle_
) {
	XioStreamFilterIpAddressState * ip_address_filter = handle_;
	int error = 0;

	if ( !handle_ ) {
		error = __LINE__;
	} else {
		ip_address_filter->ip_address[15] = '\0';
		ip_address_filter->ip_address_index = 0;
		ip_address_filter->state_machine_index = 0;
		ip_address_filter->valid_ip_address = false;
	}

	return error;
}

